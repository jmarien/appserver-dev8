#
# by @cmorisse  
#  
function remove_buildout {
	echo "Removing all buidout generated items..."
	echo "    Not removing downloads/ to avoid re-downloading openerp"
	rm -rf .installed.cfg
	rm -rf bin/
	rm -rf develop-eggs/
	rm -rf eggs/
	rm -rf etc/
	rm -rf py27/
	echo "    Done."
}  

if [ "$1" == "--reset" ]; then
	remove_buildout
	exit
fi

if [ -d py27 ]; then
	echo "install.sh has already been launched."
	echo "So you must either use bin/buildout to update or launch \"install.sh --reset\" to remove all buildout installed items."
	exit -1
fi

virtualenv py27
py27/bin/pip install -i http://openerp.inouk.fr zc.buildout==2.2.0
py27/bin/pip install -i http://openerp.inouk.fr bzr
py27/bin/buildout bootstrap

# Or anybox style
# wget https://raw.github.com/buildout/buildout/master/bootstrap/bootstrap.py
# python bootstrap.py

bin/buildout install
echo
echo "Your commands are now available in ./bin"
echo "Python is in ./py27. Don't forget to launch"
echo "source py27/bin/activate"
echo 